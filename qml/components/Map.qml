// Map.qml
// This file is part of the OpenMensa App
// Copyright (c) 2018 Jan Sprinz <jan@ubports.com>
// See Main.qml and LICENSE for more information

import QtQuick 2.9
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import QtWebEngine 1.0

WebEngineView {
    property var latitude
    property var longitude
    url: "../../html/map.html?lat=" + latitude + "&long=" + longitude
}
