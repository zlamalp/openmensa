// NearbyCanteens.qml
// This file is part of the OpenMensa App
// Copyright (c) 2018 Jan Sprinz <jan@ubports.com>
// See Main.qml and LICENSE for more information

import QtQuick 2.9
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import QtPositioning 5.2
import "../js/openmensa.js" as OpenMensa
import "components"

Page {
    id: mainPage

    header: PageHeader {
        title: i18n.tr("Nearby Canteens")
        flickable: scrollView.flickableItem

        trailingActionBar {
            actions: [
                Action {
                    text: i18n.tr("Update")
                    onTriggered: scrollView.update()
                    iconName: "reload"
                }
            ]
        }
    }

    CanteenList {
        id: scrollView
        onUpdate: {
            if (!geoposition.valid && geoposition.active &&
                (geoposition.sourceError == PositionSource.NoError)) {
                loading = true;
                setTimeout(function() {
                    update();
                }, 100);
            } else if (cache.latitude && cache.longitude) {
                loading = true;
                canteenList = []
                OpenMensa.get_near(cache.latitude, cache.longitude,
                    settings.distance,
                    function on_success(list) {
                        if (list !== []) {
                            canteenList = list;
                        } else {
                            error = true;
                            errorText = l18n.tr("There do not seem to be any canteens in your area...")                        }
                    }, function on_failure() {
                        error = true;
                        errorText = l18n.tr("Can't reach OpenMensa API.")
                    }
                );
                loading = false;
            } else {
                loading = false;
                switch (geoposition.sourceError) {
                    case PositionSource.AccessError:
                        error = true;
                        errorText = i18n.tr("Location access denied. Please make sure the app has proper permissions to access your location. If this continues to happen, you might be affected by a bug in the operating system.");
                        break;
                    case PositionSource.ClosedError:
                        error = true;
                        errorText = i18n.tr("Please enable location services on your device.");
                        break;
                    case PositionSource.UnknownSourceError:
                        error = true;
                        errorText = i18n.tr("There was an unknown source error while detecting your location. This might be caused by a bug in your operating system.");
                        break;
                    case PositionSource.SocketError:
                        error = true;
                        errorText = i18n.tr("There was an error connecting to the nmea socket while detecting your location. This might be caused by a bug in your operating system.");
                        break;
                    default:
                        error = true;
                        errorText = i18n.tr("There was an unknown error while detecting your location. This might be caused by a bug in your operating system.");
                            break;
                }
            }
        }
    }
}
