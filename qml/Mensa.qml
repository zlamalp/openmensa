// Mensa.qml
// This file is part of the OpenMensa App
// Copyright (c) 2018 Jan Sprinz <jan@ubports.com>
// See Main.qml and LICENSE for more information

import QtQuick 2.9
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import Qt.labs.settings 1.0
import "components"
import "../js/openmensa.js" as OpenMensa

Page {
    id: mensaPage

    property string name
    property int mensaId
    property string city
    property string address
    property string latitude
    property string longitude
    property var days
    property var loadingState: 0 // LOADING, CLOSED, ERROR, DONE

    header: PageHeader {
        id: header
        title: name

        trailingActionBar.actions: [
        Action {
            text: i18n.tr("About")
            onTriggered: Qt.openUrlExternally("http://map.unav.me?" + latitude + "," + longitude)
            iconName: "location"
        },
        Action {
            text: i18n.tr("Star")
            checkable: true
            checked: favourites.indexOf(mensaId) != -1
            onTriggered: {
                console.log(favourites)
                if (favourites.indexOf(mensaId) == -1) {
                    favourites.push(mensaId)
                } else {
                    favourites.splice(favourites.indexOf(mensaId),1);
                }
                console.log(favourites)
            }
            iconName: checked ? "starred" : "non-starred";
        }
        ]
    }

    Map {
        id: map
        latitude: parent.latitude
        longitude: parent.longitude
        width: parent.width
        height: parent.height / 5
        anchors.top: header.bottom
    }

    Sections {
        id: daysSelector
        anchors.top: map.bottom
        Component.onCompleted: {
            loadingState = 0;
            OpenMensa.get_days(mensaId,
                function on_success(response) {
                    var dateList = [];
                    for (var date in response) {
                        dateList[date] = Qt.formatDateTime(response[date].date, "dddd");
                    }
                    model = dateList;
                    mensaPage.days = response;
                    update_meals();
                },
                function on_failure() {
                    loadingState = 2;
                }
            );
        }
        onSelectedIndexChanged: { update_meals(); }

        function update_meals() {
            print("Loading " + mensaPage.days[selectedIndex].date);
            if (mensaPage.days[selectedIndex].closed) {
                loadingState = 1;
            } else {
                loadingState = 0;
                OpenMensa.get_meals(mensaPage.mensaId, mensaPage.days[selectedIndex].date,
                    function on_success(list) {
                        mealsList.model = list;
                        loadingState = 3;
                    }, function on_failure() {
                        loadingState = 2;
                    }
                );
            }
        }
    }

    ActivityIndicator {
        anchors.centerIn: parent
        running: loadingState == 0;
    }

    Label {
        anchors.centerIn: parent
        visible: loadingState == 1;
        text: i18n.tr("Closed :/")
    }

    Label {
        anchors.centerIn: parent
        visible: loadingState == 2;
        text: i18n.tr("Error :'(")
    }

    ListView {
        id: mealsList
        anchors.left: parent.left
        anchors.top: daysSelector.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        property var day
        visible: loadingState == 3;
        delegate: ListItem {
            ListItemLayout {
                anchors.centerIn: parent

                title.text: modelData.name
                subtitle.text: modelData.category
                summary.text: modelData.notes.join()

                Label {
                    visible: price != null
                    property var price: settings.price == 0 && modelData.prices.students ?
                        modelData.prices.students :
                        settings.price == 1 && modelData.prices.employees ?
                        modelData.prices.employees :
                        settings.price == 2 && modelData.prices.pupils ?
                        modelData.prices.pupils :
                        modelData.prices.others
                    text: price + " EUR"
                    SlotsLayout.position: SlotsLayout.Last
                }
            }
        }
    }

    ScrollView {
        id: scrollView
        anchors.fill: parent
    }
}
